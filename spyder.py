from db_storage import LoaderDBPages, FillerDBPages
import argparse

def createParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('action')
    parser.add_argument('url')
    parser.add_argument('-d', '--depth')
    parser.add_argument('-i', '--items')

    return parser

if __name__ == '__main__':
    parser = createParser()
    namespace = parser.parse_args()

    if namespace.action == 'get':
        loader = LoaderDBPages('test_db.db')
        data = loader.call(namespace.url, namespace.items)
        for row in data:
            print(row[1], row[0])

    elif namespace.action == 'load':
        db_filler = FillerDBPages('test_db.db')
        db_filler.call(namespace.url, namespace.depth)
    else:
        print('неизвестное действие')
