import re


class HrefItem:
    def __init__(self, href_str):
        self.__href_str = href_str
        self.__title = ''
        self.__url = ''
        self.__parse_href_info()

    @property
    def title(self):
        return self.__title

    @property
    def url(self):
        return self.__url

    def is_external_link(self):
        result = re.search(r'rel="external"', self.__href_str)
        return not(result is None)

    def is_empty_link(self):
        return (self.title == '') | (self.url == '')

    def __parse_href_info(self):
        self.__href = self.__href_str.replace('&nbsp;', ' ')
        result = re.search(r'href=\"(?P<url>http.*?)\".*>(?P<title>.*)', self.__href_str)
        if result is None:
            return None

        self.__title = result.group('title').strip()
        self.__url = result.group('url').strip()


class HrefList:
    def __init__(self, href_list):
        self.__fill_items(href_list)

    def __iter__(self):
        self.__idx = 0
        return self

    def __next__(self):
        if self.__idx > len(self.__href_items) - 1:
            raise StopIteration
        else:
            item = self.__href_items[self.__idx]
            self.__idx += 1
            return item

    @property
    def href_items(self):
        return self.__href_items

    def __fill_items(self, href_list):
        self.__href_items = []
        for href in href_list:
            href_item = HrefItem(href)

            if href_item.is_external_link() | href_item.is_empty_link():
                continue

            self.__href_items.append(href_item)
        return len(self.href_items)

