from urllib.request import urlopen
import urllib.error
import re
from href_item import HrefList


class HtmlPage:
    def __init__(self, url, title):
        self.__url = url
        self.__title = title
        self.__html = ''
        self.__href_list = None
        self.__loaded = False

    @property
    def loaded(self):
        return self.__loaded

    @property
    def title(self):
        return self.__title

    @property
    def url(self):
        return self.__url

    @property
    def html(self):
        return self.__html

    @property
    def href_list(self):
        return self.__href_list

    def load(self):
        if self.loaded:
            return self

        self.__html = ''

        try:
            with urlopen(self.url) as site:
                try:
                    self.__html = site.read().decode('utf8')
                except UnicodeDecodeError:
                    pass
        except urllib.error.HTTPError:
            pass

        self.__loaded = True
        return self

    def parse_urls(self):
        header_tags = re.findall(r'<a (.*?)</a>', str(self.html))
        self.__href_list = HrefList(header_tags)
        return self


class HtmlPagesList:
    def __init__(self, parent_page, depth):
        self.__pages = []
        self.__depth = depth
        self.__parent_page = parent_page
        if depth > 0:
            self.__build_pages_list()

    def __iter__(self):
        self.__idx = 0
        return self

    def __next__(self):
        if self.__idx > len(self.__pages) - 1:
            raise StopIteration
        else:
            item = self.__pages[self.__idx]
            self.__idx += 1
            return item

    def __len__(self):
        return len(self.__pages)

    @property
    def pages(self):
        return self.__pages

    @property
    def parent_page(self):
        return self.__parent_page

    def load_pages(self):
        for page in self.pages:
            page.load()
        return self

    def __build_pages_list(self):
        self.parent_page.load().parse_urls()

        for href_item in self.parent_page.href_list:
            page = HtmlPage(href_item.url, href_item.title)
            self.__pages.append(page)

        return self