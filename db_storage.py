from html_page import HtmlPage, HtmlPagesList
import sqlite3

class InitDBTable:
    def __init__(self, connect):
        self.__connect = connect
        self.__cursor = connect.cursor()

    def call(self):
        self.__create_table()
        self.__create_index()
        self.__connect.commit()

    def __create_table(self):
        create_table_str = """
          CREATE TABLE IF NOT EXISTS pages (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            main_page_id INTEGER,
            title CHAR(255),
            url CHAR(255) NOT NULL,
            html TEXT);
        """
        self.__cursor.execute(create_table_str)

    def __create_index(self):
        self.__cursor.execute("CREATE INDEX IF NOT EXISTS main_page_id_index ON pages (main_page_id);")
        self.__cursor.execute("CREATE INDEX IF NOT EXISTS url_title_index ON pages (title, url);")

class BaseDBClass:
    def __init__(self, file_name):
        self.__file_name = file_name
        self.__connection = None
        self.__cursor = None

    @property
    def connection(self):
        return self.__connection

    @property
    def cursor(self):
        return self.__cursor

    def open_connection(self):
        self.__connection = sqlite3.connect(self.__file_name)
        self.__cursor = self.__connection.cursor()

    def close_connection(self):
        self.connection.close()


class FillerDBPages(BaseDBClass):
    def __init__(self, file_name):
        super().__init__(file_name)
        self.start_url = ''
        self.main_page_id = 0

        self.open_connection()
        InitDBTable(self.connection).call()
        self.close_connection()

    def call(self, start_url, depth):
        self.start_url = start_url

        self.open_connection()

        main_page = HtmlPage(start_url, '').load()
        self.main_page_id = self.__save_if_not_saved(main_page)

        self.__save_sub_pages(main_page, depth)

        self.connection.commit()
        self.close_connection()

    def __save_page(self, page):
        query_str = "INSERT INTO pages (main_page_id, title, url, html) VALUES (?, ?, ?, ?);"
        result = self.cursor.execute(query_str, (self.main_page_id, page.title, page.url, page.html))
        self.connection.commit()
        return result.lastrowid

    def __get_page_id(self, page):
        query_str = "SELECT id FROM pages WHERE title = ? AND url = ? LIMIT 1;"
        self.cursor.execute(query_str, (page.title, page.url))
        data = self.cursor.fetchall()
        if len(data) > 0:
            return data[0][0]
        else:
            return -1

    def __save_if_not_saved(self, page):
        page_id = self.__get_page_id(page)
        if page_id <= 0:
            page_id = self.__save_page(page)

        return page_id

    def __save_sub_pages(self, page, depth):
        self.__save_if_not_saved(page)
        p_list = HtmlPagesList(page, depth)

        if len(p_list) <= 0:
            return

        for p in p_list:
            self.__save_sub_pages(p, depth - 1)


class LoaderDBPages(BaseDBClass):
    def __init__(self, file_name):
        super().__init__(file_name)
        self.__start_url = ''
        self.__item_amount = 0
        self.__data = None

    @property
    def data(self):
        return self.__data

    def call(self, start_url, item_amount):
        self.__start_url = start_url
        self.__item_amount = item_amount

        self.open_connection()
        self.__select_items()
        self.close_connection()

        return self.data

    def __select_items(self):
        query_str = """
            SELECT title, url FROM pages
            WHERE main_page_id = (
              SELECT id FROM pages
              WHERE  url = ?)
            LIMIT ?;
        """

        self.cursor.execute(query_str, (self.__start_url, self.__item_amount))
        self.__data = self.cursor.fetchall()